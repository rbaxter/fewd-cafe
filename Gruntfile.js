module.exports = function(grunt) {

  //Project Config
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      dev: {
        options: {
          livereload: 35729
        },
        files: ['app/style/*.less', 'app/index.html', 'app/js/*.js', 'app/partials/**/*.js', 'app/partials/**/*.html'],
        tasks: ['less:dev']
      }
    },

    connect: {
      dev: {
        options: {
          hostname: 'localhost',
          port: 8080,
          base: 'app'
        }
      }
    },

    less: {
      dev: {
        files: {'app/style/compiled.css': 'app/style/main.less'}
      }
    }

  });

  //Project Information
  grunt.task.registerTask('project_info', 'Project Information', function(){
    var project = grunt.file.readJSON('package.json');
    grunt.log.writeln('----------------------------------------'.rainbow);
    grunt.log.writeln(' ' + project.name );
    grunt.log.writeln('----------------------------------------'.rainbow);
  });

  //Tasks! `grunt <task>`
  grunt.task.registerTask('default', ['project_info', 'less:dev', 'connect:dev', 'watch:dev']);

  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');


};