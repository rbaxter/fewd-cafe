'use strict';

(function() {
	var app = angular.module('fewdCafe', [
		'ngRoute',
		'fewdCafe.navBar',
		'fewdCafe.starRating',
		'fewdCafe.today',
		'fewdCafe.yesterday',
		'fewdCafe.review'
	])
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.otherwise({redirectTo: '/today'});
	}]);

	app.factory('TodaysFood', ['$firebaseObject', function($firebaseObject) {
		var today = moment().format('YYYY-MM-DD');
		var ref   = new Firebase("https://scorching-fire-5851.firebaseio.com/foods/" + today);
		var food  = $firebaseObject(ref);

		return food;
	}]);

	app.factory('YesterdaysFood', ['$firebaseObject', function($firebaseObject) {
		var yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
		var ref       = new Firebase("https://scorching-fire-5851.firebaseio.com/foods/" + yesterday);
		var food      = $firebaseObject(ref);

		return food;
	}]);

	app.factory('YesterdaysRatings', ['$firebaseObject', function($firebaseObject) {
		var yesterday = moment().subtract(1, 'days').format('YYYY-MM-DD');
		var ref       = new Firebase("https://scorching-fire-5851.firebaseio.com/cafeRatings/" + yesterday);
		var ratings   = $firebaseObject(ref);

		return ratings;
	}]);

})();
