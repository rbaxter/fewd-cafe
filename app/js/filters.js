'use strict';

(function() {
	var app = angular.module('fewdCafe');

	app.filter('percent', ['$filter', function($filter) {
		return function (input, decimals) {
			decimals = parseInt(decimals, 10);
			decimals = (!isNaN(decimals) ? decimals : 0);
			input    = parseFloat(input * 100, 10);

			return (!isNaN(input) ? parseFloat(input.toFixed(decimals), 10) + '%' : '');
		};
	}]);

})();