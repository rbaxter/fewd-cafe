'use strict';

(function() {
	var app = angular.module('fewdCafe.navBar', [])
	.controller('NavCtrl', ['$scope', '$location', function($scope, $location) {
		 $scope.isActive = function (viewLocation) { 
	        return viewLocation === $location.path();
	    };
	}]);

	app.directive('navBar', function() {
		return {
			restrict: 'E',
			templateUrl: 'partials/navbar/navbar.html'
		};
	});
})();