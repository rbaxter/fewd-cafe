'use strict';

(function() {
	var app = angular.module('fewdCafe.review', ['ngRoute', 'firebase'])
	 
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/review/:id', {
			templateUrl: 'templates/review/review.html'
		});
	}]);

	app.controller("ReviewCtrl", ['$scope', '$routeParams', '$location', '$firebaseObject', function($scope, $routeParams, $location, $firebaseObject) {
		//check to see if they've already reviewed a food today
		var today = moment().format('YYYY-MM-DD');
		var alreadyReviewed = window.localStorage.getItem('lastReviewedDate');

		if(alreadyReviewed == today) {
			console.log("You already reviewed today");
		}

		if ($routeParams.id && alreadyReviewed != today) {
			var ref         = new Firebase("https://scorching-fire-5851.firebaseio.com/foods/" + today);
			var foodItem    = $firebaseObject(ref.child($routeParams.id));
			var reviewRef 	= new Firebase("https://scorching-fire-5851.firebaseio.com/cafeRatings/" + today);
			$scope.foodItem = foodItem;
			console.log("you can review");

			foodItem.$loaded().then(function(){
				$scope.review = {
					campus: foodItem.campus,
					comment: '',
					food: foodItem.id,
					rating : 1
				};
			});
		} else {
			$location.path('/today');
		}

    	//submit a review to the database
		$scope.addReview = function() {
			reviewRef.push($scope.review);
			$location.path('/today');
			window.localStorage.setItem('lastReviewedDate', today);
		};
	}]);

})();