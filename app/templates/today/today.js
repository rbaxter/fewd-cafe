"use strict";

(function() {
	var app = angular.module('fewdCafe.today', ['ngRoute', 'firebase'])

	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/today', {
			templateUrl: 'templates/today/today.html'
		});
	}]);

	app.controller("TodayCtrl", ['$scope', '$location', 'TodaysFood', function($scope, $location, TodaysFood) {
		$scope.menu = TodaysFood;
		$scope.dayOfWeek = moment().format('dddd');
		$scope.isLoading = true;

		var countFoodItems = function() {
			$scope.isLoading = false;
			$scope.numItems = 0;
			TodaysFood.forEach(function(value, key){
				$scope.numItems++;
			});
		};

		TodaysFood.$watch(countFoodItems);
		TodaysFood.$loaded().then(countFoodItems);

		//set error text if menu can't load
		if ($scope.dayOfWeek == "Saturday" || $scope.dayOfWeek == "Sunday") {
			$scope.errorText = "Sorry! We don't serve lunch on " + $scope.dayOfWeek + "s. Please try again during the week.";
		} else {
			$scope.errorText = "There was a problem loading the menu. Please try again later.";
		}

		$scope.reviewThis = function( id ) {
			$location.path('/review/' + id);
		};
	}]);
})();