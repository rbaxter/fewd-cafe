'use strict';

(function() {
	var app = angular.module('fewdCafe.yesterday', ['ngRoute'])
	 
	.config(['$routeProvider', function($routeProvider) {
	    $routeProvider.when('/yesterday', {
	        templateUrl: 'templates/yesterday/yesterday.html'
	    });
	}]);

	app.controller("YesterdayCtrl", ['$scope', '$location', 'YesterdaysRatings', 'YesterdaysFood', function($scope, $location, YesterdaysRatings, YesterdaysFood) {
		var ratingsLoaded = false;
		var foodLoaded = false;

		var mungeRecords = function() {
			var ratedFoods   = {};
			var totalRatings = 0;

			if (ratingsLoaded && foodLoaded) {
				YesterdaysFood.forEach(function(food, id){
					ratedFoods[id] = {
						name: food.name,
						avgRating: 0,
						ratingCount: 0,
						comments: []
					};
				});

				YesterdaysRatings.forEach(function(rating, id){
					var food = ratedFoods[rating.food];
					if ( food ) {
						food.avgRating += rating.rating;
						food.comments.push(rating.comment);
						food.ratingCount++;
						totalRatings++;
					}
				});

				for (var foodID in ratedFoods) {
					if (ratedFoods[foodID].ratingCount) {
						ratedFoods[foodID].avgRating         = ratedFoods[foodID].avgRating / ratedFoods[foodID].ratingCount;
						ratedFoods[foodID].scorePercentage   = ratedFoods[foodID].avgRating / 5;
						ratedFoods[foodID].percentageOfTotal = ratedFoods[foodID].ratingCount / totalRatings;
					}
				}

				$scope.ratedFoods = ratedFoods;

			}
		};

		YesterdaysRatings.$loaded().then(function(){
			ratingsLoaded = true;
			mungeRecords();
		});

		YesterdaysFood.$loaded().then(function(){
			foodLoaded = true;
			mungeRecords();
		});

		YesterdaysRatings.$watch(function(){
			ratingsLoaded = true;
			mungeRecords();
		});

		YesterdaysFood.$watch(function(){
			foodLoaded = true;
			mungeRecords();
		});
	}]);
})();